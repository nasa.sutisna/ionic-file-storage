import { Component, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Camera } from '@ionic-native/camera';
import { IonicPage, NavController, ViewController } from 'ionic-angular';
import { File } from '@ionic-native/file';
var win: any = window;
@IonicPage()
@Component({
  selector: 'page-item-create',
  templateUrl: 'item-create.html'
})
export class ItemCreatePage {
  @ViewChild('fileInput') fileInput;

  isReadyToSave: boolean;

  item: any;

  form: FormGroup;
  photo: any;
  constructor(
    public navCtrl: NavController,
    public viewCtrl: ViewController,
    formBuilder: FormBuilder,
    public camera: Camera,
    public file: File
  ) {
    this.form = formBuilder.group({
      profilePic: [''],
      name: ['', Validators.required],
      about: ['']
    });

    // Watch the form for changes, and
    this.form.valueChanges.subscribe((v) => {
      this.isReadyToSave = this.form.valid;
    });
  }

  ionViewDidLoad() {

  }

  getPicture() {
    if (Camera['installed']()) {
      this.camera.getPicture({
        destinationType: this.camera.DestinationType.FILE_URI,
        targetWidth: 96,
        targetHeight: 96
      }).then(async (data) => {
        console.log(data);
        this.photo = win.Ionic.WebView.convertFileSrc(data);
        console.log('photo', this.photo);

        let oldPath = encodeURI(data.replace(data.substr(data.lastIndexOf('/') + 1), ""));
        let oldFileName = data.substr(data.lastIndexOf('/') + 1);
        let newPath = await this.getUrlDirectory();
        let newFilename = 'new_' + new Date().getTime() + '.jpg';

        this.form.patchValue({ 'profilePic': 'data:image/jpg;base64,' + data });

        this.file.copyFile(oldPath, oldFileName, newPath.nativeUrl, newFilename)
          .then((result) => {
            console.log(result);
            this.file.removeFile(oldPath, oldFileName).then((result) => {
              console.log('remove file', result)
              const nativeUrl = newPath.nativeUrl
              let path = nativeUrl.substr(0, nativeUrl.lastIndexOf('/attendance') + 1);
              console.log('path', path)
              this.file.listDir(path, 'attendance')
                .then((list) => {
                  console.log('path url', newPath.nativeUrl);
                  console.log('list dir', list);
                  this.file.getFreeDiskSpace()
                    .then((val) => {
                      console.log('free disk : ' + this.formatBytes(val))
                    })
                })
                .catch((error) => {
                  console.log('list dir error', error)
                })
            }).catch(error => {
              console.log('remove file error', error)
            });
          })
          .catch((error) => {
            console.log('copy path error', error)
          })
      }, (err) => {
        alert('Unable to take photo');
      })
    } else {
      this.fileInput.nativeElement.click();
    }
  }

  getUrlDirectory() {
    return this.checkDirectory().then((val) => {
      return Promise.resolve(({
        nativeUrl: this.file.dataDirectory + 'attendance/'
      }))
    })
  }

  checkDirectory() {
    return this.file.checkDir(this.file.dataDirectory, 'attendance')
      .then(value => {
        console.log(value);
        return value;
      })
      .catch(err => {
        console.log(err, ' error')
        return this.file.createDir(this.file.dataDirectory, 'attendance', true)
      });
  }

  formatBytes(bytes, decimals = 2) {
    if (bytes === 0) return '0 Bytes';

    const k = 1024;
    const dm = decimals < 0 ? 0 : decimals;
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
    const i = Math.floor(Math.log(bytes) / Math.log(k));

    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
  }

  clearStorage() {
    this.file.removeRecursively(this.file.dataDirectory, 'attendance')
    .then((result) => {
      console.log('remove recursive', result);
      this.file.listDir(this.file.dataDirectory, '')
      .then((list) => {
        console.log('list file',list)
      })
    })
    .catch((error) => {
      console.log('remove recursice error',error);

    })
  }

  processWebImage(event) {
    let reader = new FileReader();
    reader.onload = (readerEvent) => {

      let imageData = (readerEvent.target as any).result;
      this.form.patchValue({ 'profilePic': imageData });
    };

    reader.readAsDataURL(event.target.files[0]);
  }

  getProfileImageStyle() {
    return 'url(' + this.form.controls['profilePic'].value + ')'
  }

  /**
   * The user cancelled, so we dismiss without sending data back.
   */
  cancel() {
    this.viewCtrl.dismiss();
  }

  /**
   * The user is done and wants to create the item, so return it
   * back to the presenter.
   */
  done() {
    if (!this.form.valid) { return; }
    this.viewCtrl.dismiss(this.form.value);
  }
}
