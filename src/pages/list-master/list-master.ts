import { Component, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Camera } from '@ionic-native/camera';
import { IonicPage, NavController, ViewController, Platform } from 'ionic-angular';
import { File } from '@ionic-native/file';
var win: any = window;

@IonicPage()
@Component({
  selector: 'page-list-master',
  templateUrl: 'list-master.html'
})
export class ListMasterPage {
  @ViewChild('fileInput') fileInput;

  isReadyToSave: boolean;

  item: any;

  form: FormGroup;
  photo: any;
  freeDisk: any = 0;
  fileList: Array<any> = []
  constructor(
    public navCtrl: NavController,
    public viewCtrl: ViewController,
    public camera: Camera,
    public file: File,
    public platform: Platform
  ) {

  }

  async ngOnInit() {
    await this.platform.ready();
    if (this.platform.is('cordova')) {
      this.getListFile();
      this.getFreeDisk();
    }
  }

  ionViewDidLoad() {

  }

  showPhoto(fullPath) {
    return win.Ionic.WebView.convertFileSrc(fullPath);
  }

  getPicture() {
    if (Camera['installed']()) {
      this.camera.getPicture({
        destinationType: this.camera.DestinationType.FILE_URI,
        targetWidth: 100,
        targetHeight: 100,
        correctOrientation: true
      }).then(async (data) => {
        console.log(data);
        this.photo = win.Ionic.WebView.convertFileSrc(data);
        console.log('photo', this.photo);

        let oldPath = encodeURI(data.replace(data.substr(data.lastIndexOf('/') + 1), ""));
        let oldFileName = data.substr(data.lastIndexOf('/') + 1);
        let newPath = await this.getUrlDirectory();
        let newFilename = 'new_' + new Date().getTime() + '.jpg';

        this.file.copyFile(oldPath, oldFileName, newPath.nativeUrl, newFilename)
          .then((result) => {
            console.log(result);
            this.file.removeFile(oldPath, oldFileName).then((result) => {
              console.log('remove file', result);
              this.getListFile();
            }).catch(error => {
              console.log('remove file error', error)
            });
          })
          .catch((error) => {
            console.log('copy path error', error)
          })
      }, (err) => {
        alert('Unable to take photo');
      })
    } else {
      this.fileInput.nativeElement.click();
    }
  }

  getUrlDirectory() {
    return this.checkDirectory().then((val) => {
      return Promise.resolve(({
        nativeUrl: this.file.dataDirectory + 'attendance/'
      }))
    })
  }

  checkDirectory() {
    return this.file.checkDir(this.file.dataDirectory, 'attendance')
      .then(value => {
        console.log(value);
        return value;
      })
      .catch(err => {
        console.log(err, ' error')
        return this.file.createDir(this.file.dataDirectory, 'attendance', true)
      });
  }

  getFreeDisk() {
    this.file.getFreeDiskSpace()
      .then((val) => {
        console.log('free disk', val);
        this.freeDisk = this.formatBytes(val)
      })
  }

  getListFile() {
    this.file.listDir(this.file.dataDirectory, 'attendance')
      .then((list) => {
        console.log('list', list);
        console.log('native', list[0]['nativeURL']);
        this.fileList = list.reverse();
        this.file.listDir(this.file.dataDirectory, '')
          .then((list) => {
            console.log('list dir 1', list)
          })
      })
      .catch((error) => {
        console.log('list dir error', error)
      })
  }

  formatBytes(bytes, decimals = 2) {
    if (bytes === 0) return '0 Bytes';

    const k = 1024;
    const dm = decimals < 0 ? 0 : decimals;
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
    const i = Math.floor(Math.log(bytes) / Math.log(k));

    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
  }

  clearStorage() {
    this.file.removeRecursively(this.file.dataDirectory, 'attendance')
      .then((result) => {
        console.log('remove recursive', result);
        this.getListFile();
        this.fileList = [];
        this.file.listDir(this.file.dataDirectory, '')
          .then((list) => {
            console.log('list dir', list)
          })
      })
      .catch((error) => {
        console.log('remove recursice error', error);

      })
  }

  processWebImage(event) {
    let reader = new FileReader();
    reader.onload = (readerEvent) => {

      let imageData = (readerEvent.target as any).result;
      this.form.patchValue({ 'profilePic': imageData });
    };

    reader.readAsDataURL(event.target.files[0]);
  }

  getProfileImageStyle() {
    return 'url(' + this.form.controls['profilePic'].value + ')'
  }

  /**
   * The user cancelled, so we dismiss without sending data back.
   */
  cancel() {
    this.viewCtrl.dismiss();
  }

  /**
   * The user is done and wants to create the item, so return it
   * back to the presenter.
   */
  done() {
    if (!this.form.valid) { return; }
    this.viewCtrl.dismiss(this.form.value);
  }
}
